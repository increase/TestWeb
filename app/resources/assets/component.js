$(function() {
  $('.tabs').each(function() {
    var $content, $nav, $this;
    $this = $(this);
    $nav = $this.find('> ul.tab-nav > li');
    $content = $this.children('.tab-content');
    $nav.children('a').on("click", function() {
      var $index;
      $index = $(this).parent().index();
      if ($nav.eq($index).hasClass('active')) {
        return;
      }
      $nav.add($content).removeClass('active');
      $nav.eq($index).add($content.eq($index)).addClass('active');
    });
  });

  $('input[type=radio], input[type=checkbox]').each(function () {
      var $this;
      $this = $(this);
      //its already there
      if($this.next().hasClass("form-control")){
          return;
      }
      //insert the span
      $this.after("<span class='form-control'></span>");
  });

});
