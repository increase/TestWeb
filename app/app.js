/*jslint vars: true, forin: true, plusplus: true */
/*global angular: false, engine: false*/
var probe = angular.module("probe", ['ui.router', 'angular-draggable', 'probe-engine', 'ng-context-menu']);

probe.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
	'use strict';
	var view_path = "app/view/";

	$urlRouterProvider.when("/project", "/project/index").otherwise("/auth");

	$stateProvider.state("auth", {
		url: "/auth",
		views: {
			"probe": {
				templateUrl: view_path + "auth.html"
			}
		}
	}).state("panel", {
		url: "/panel",
		views: {
			"probe": {
				templateUrl: view_path + "panel.html"
			}
		}
	}).state("panel.process", {
		url: "/process/:action",
		views: {
			"dialog": {
				templateUrl: view_path + "process.edit.html"
			}
		}
	}).state("panel.process.dictionary", {
		url: "/dictionary",
		views: {
			"dictionary": {
				templateUrl: view_path + "dictionary.html"
			}
		}
	}).state("panel.step", {
		url: "/step/:action/:index",
		views: {
			"dialog": {
				templateUrl: view_path + "step.edit.html"
			}
		}
	}).state("panel.open", {
		url: "/open",
		views: {
			"dialog": {
				templateUrl: view_path + "process.open.html"
			}
		}
	}).state("panel.about",{
		url:"/about",
		views:{
			"dialog":{
				templateUrl: view_path + "about.html"
			}
		}
	})
});
