var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var paths = {
	scripts: [
		'./bower_components/jquery/dist/jquery.js',
		'./bower_components/angular/angular.js',
		'./bower_components/angular-ui-router/release/angular-ui-router.js',
		'./bower_components/angular-draggable/js/main.js',
        './bower_components/ng-context-menu/dist/ng-context-menu.js'
	],
	sass: ['./app/resources/sass/*.scss'],
	css: ['./app/resources/css/debug.css'],
	html: ['./app/view/*.html'],
	js: ['./app/app.js', './app/controller.js']
};

gulp.task('js',  function() {
	// Minify all JavaScript
	// with sourcemaps all the way down
	return gulp.src(paths.scripts)
		.pipe(uglify())
		.pipe(concat('lib.js'))
		.pipe(gulp.dest('./lib'));
});

gulp.task('sass', function () {
	return gulp.src(['./app/resources/sass/style.scss'])
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./app/resources/css'))
		.pipe(reload({stream:true}));
});

gulp.task('browser-sync', function () {
	browserSync({
		server: {
			baseDir: "./"
		},
		startPath: "index.html"
	});
	//reload html,css,js on change
	gulp.watch(paths.html).on('change', reload);
	gulp.watch(paths.css).on('change', reload);
	gulp.watch(paths.js).on('change', reload);
});

gulp.task('watch', function() {
	gulp.watch(paths.sass, ['sass']);
});

gulp.task('default', ['browser-sync', 'watch']);