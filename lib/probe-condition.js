/*jslint vars: true, forin: true */
/*global angular: false, engine: false*/
engine.factory("Condition", function () {
		/**
	 * As the execution conditions of the EventStep.
	 *
	 * @class Condition
	 * @constructor
	 * @param object {string} CSSpath string of object.
	 */
	var Condition = function Condition(object) {
		/**
		 * The object(HTMLElement) to check. The param is CSSPath.
		 * @property object
		 * @type string
		 */
		this.object = object;
	};
	/**
	 * Test if the object existed.
	 * @method test
	 * @returns {boolean} The object is existed?
	 */
	Condition.prototype.test = function () {
		return ($(this.object, window[0].document).length > 0);
	};
	/**
	 * Override default toString.
	 * @method toString
	 * @returns {string} The CSSPath string of object.
	 */
	Condition.prototype.toString = function () {
		return this.object;
	};

	return Condition;
});
