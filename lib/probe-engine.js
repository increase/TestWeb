/*jslint vars: true, forin: true */
/*global angular: false */
var engine = angular.module("probe-engine", []).config(['$provide', function ($provide) {
	'use strict';
	var HEAD = {};

	$provide.factory("Engine", ['DelayStep', 'EventStep', 'Result', 'Dictionary', 'Condition', 'Process',
		function (DelayStep, EventStep, Result, Dictionary, Condition, Process) {
			var product = {
				DelayStep: DelayStep,
				EventStep: EventStep,
				Dictionary: Dictionary,
				Process: Process
			};

			return {
				create: function (type, options) {
					return new product[type](options);
				},
				// 将HEAD的Process对象翻译成json对象
				toJson: function (entity) {
					return HEAD.toJson;
				},
				// 将指定的Json对象转换为Process对象
				fromJson: function (entity) {

				},
				HEAD: function (process) {
					return angular.isDefined(process) ? (HEAD = process) : HEAD;
				},
				hasHEAD: function () {
					return HEAD instanceof Process;
				}
			};
	}]);

	$provide.factory("Player", function () {
		// 播放器服务
		var play_queue = [];
		var play_state = "stop";

		return {
			play: function () {
				if (play_state === "stop") {
					HEAD.play();
					play_state = "play";
				} else if (play_state === "pause") {
					HEAD.resume();
					play_state = "play";
				}
			},
			pause: function () {
				if (play_state === "play") {
					HEAD.pause();
					play_state = "pause";
				}
			},
			stop: function () {
				HEAD.stop();
				play_state = "stop";
			},
			step: function () {
				HEAD.step();
			},
			reset: function () {
				HEAD.reset();
			}
		};
	});

}]);
