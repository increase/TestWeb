/*jslint vars: true, forin: true */
/*global angular: false, engine: false*/
engine.factory("DelayStep", function (Step, config) {
	'use strict';
	/**
	 * DelayStep extend from Step. This kind of Step will
	 * run after a period of time(delay).
	 *
	 * @class DelayStep
	 * @extends Step
	 * @constructor
	 */
	var DelayStep = function DelayStep(options) {
		/**
		 * If no param input, make time be DEFAULT_DELAY .
		 * @property delay
		 * @type number
		 */
		this.delay = options.delay;
		/**
		 * Cache the ID of timeout timer (id of setTimeout()).
		 * @property timer_id
		 * @type number
		 */
		this.timer_id = 0;

		Step.call(this, options);
	};
	DelayStep.prototype = new Step();
	DelayStep.prototype.constructor = DelayStep;
	DelayStep.prototype.run = function (callback) {
		var that = this;
		that.timer_id = setTimeout(
			function () {
				if (that.$run()) {
					if (typeof callback === "function") {
						callback.call(that.$process, 1);
					}
					that.$process.$core();
				} else {
					if (typeof callback === "function") {
						callback.call(that.$process, 0);
					}
				}
			},
			// Control the real delay by DELAY_RATIO.
			this.delay /* * (config.get("DELAY_RATIO_MODE") ? config.get("DELAY_RATIO") : 1.0)*/
		);
	};
	DelayStep.prototype.abort = function () {
		var timer_id = this.timer_id;
		clearTimeout(timer_id);
	};
	DelayStep.prototype.toOptions = function () {
		var options = {};
		angular.forEach(Step.prototype.toOptions.call(this), function (value, key) {
			this[key] = value;
		}, options);
		options.delay = this.delay;
		options.type = "DelayStep";

		return options;
	};
	DelayStep.prototype.toJson = function () {
		var json = Step.prototype.toJson.call(this);
		json.type = "DelayStep";
		json.typeparam = angular.toJson({
			delay: this.delay
		});
		return json;
	};

	return DelayStep;
});
