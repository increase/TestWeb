/*jslint vars: true, forin: true */
/*global angular: false, engine: false*/
engine.factory("Entity", function () {
	'use strict';
	/**
	 * 每一个该应用中的实体都继承自这个类，用于管理与应用不相关的信息。
	 * 另外，该构造函数提供基本的Info与Property的存取器方法。
	 *
	 * @class Entity
	 * @constructor
	 * @param object
	 */
	var Entity = function Entity(options) {
		options = options || {};
		var time = (new Date).getTime();
		/**
		 * 与应用核心无关的必要信息由Entity管理在info属性里。
		 * @property info
		 * @type object
		 */
		this.info = {
			// 如果不存在ID说明该实例在服务端不存在
			ID: options.ID || "",
			// 名字不允许为空
			name: options.name || "Warning: name undefined!",
			comment: options.comment || "",
			// 更新时间与创建时间由服务端确定
			updatetime: options.updatetime || time,
			createtime: options.createtime || time
		};
	};
	/**
	 * 设置info的数据
	 * @method setInfo
	 * @param key {string} 键
	 * @param value 值
	 * @returns {Entity} this
	 */
	Entity.prototype.setInfo = function (key, value) {
		if (this.info.hasOwnProperty(key)) {
			this.info[key] = value;
		}
		return this;
	};
	/**
	 * 获取info的数据
	 * @method setInfo
	 * @param key {string} 键
	 * @returns value
	 */
	Entity.prototype.getInfo = function (key) {
		if (this.info.hasOwnProperty(key)) {
			return this.info[key];
		}
		return undefined;
	};
	/**
	 * 设置Entity对象的属性
	 * @method setProperty
	 * @param key {string} 键
	 * @param value 值
	 * @returns {Entity} this
	 */
	Entity.prototype.setProperty = function (key, value) {
		if (this.hasOwnProperty(key) && key !== "info") {
			this[key] = value;
		}
		return this;
	};
	/**
	 * 设置Entity对象的属性
	 * @method getProperty
	 * @param key {string} 键
	 * @returns value
	 */
	Entity.prototype.getProperty = function (key) {
		if (this.hasOwnProperty(key) && key !== "info") {
			return this[key];
		}
		return undefined;
	};
	Entity.prototype.$modifyInfo = function (options) {
		var key;
		for (key in options) {
			if (this.info.hasOwnProperty(key)) {
				this.info[key] = options[key];
			}
		}

		return this;
	};
	Entity.prototype.modify = function (options) {
		var key;
		delete options.info;
		for (key in options) {
			if (!this.info.hasOwnProperty(key) && this.hasOwnProperty(key)) {
				this[key] = options[key];
			}
		}
		this.$modifyInfo(options);

		return this;
	};
	Entity.prototype.toOptions = function () {
		return this.info;
	};

	return Entity;
});
