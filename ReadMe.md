#Orange ( Testweb )

#Recruit & Employ
```js
/*!
// Or-change HR for Javascript engineer v1.0
// http://or-change.cn
// Copyright 2014 OrChange Inc. All rights reserved.
// Licensed under the MIT License.
*/
(function (my_skills) {
	var main_condition = ["熟练掌握Javascript语言特性", "有一定前端开发经验", "熟练使用版本控制工具", "津京地区", "最好有开源作品"];
	var skill_list = "Angular Backbone Console Dir Express Fork Grunt Haslayout Iconfont Jsonp Kissy Localstorage Media query Npm Opacity Prototype uerystring Referer Seajs Trim Underscore Vim Worker Xss Yslow Zepto";
	var len = skill_list.split(" ").length; //26
	var contact_info = "hr@or-change.cn";
	
	// Method to check skills.
	var isPass = function (your_skills /* an Array */ ) {
		var i, sum = 0,
			len = your_skills.length;
		// Have a look how much you have.
		for (i = 0; i < len; i++) {
			if (skill_list.indexOf(your_skills[i]) !== -1) {
				sum += 1;
			}
		}
		// My standards.
		if (sum >= 20) {
			console.log("Please contact me!!!");
			return true;
		} else {
			console.log("Hey man, you are good");
			return false;
		}
	}
	// What need you do.
	if (isPass(my_skills)) {
		you.sendMail(contact_info);
	}
	else{
		console.log("You need go to read more books and code more program.");	
	}
}(my_skills));
```
976560